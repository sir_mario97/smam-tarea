#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: monitor.py
# Capitulo: 3 Estilo Publica-Subscribe
# Autor <StarLab> :
#   -Brenda Yasmin Barrios Becerra
#   -Paola Torres Macías
#   -Mario Alberto Muro Barraza
#  Autores Iniciales: Perla Velasco & Yonathan Mtz.
# Version: 2.1.0 Abril 2019
# Descripción:
#
#   Ésta clase define el rol del monitor, es decir, muestra datos, alertas y advertencias sobre los signos vitales de los adultos mayores.
#
#   Las características de ésta clase son las siguientes:
#
#                                            monitor.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |        Monitor        |  - Mostrar datos a los  |         Ninguna        |
#           |                       |    usuarios finales.    |                        |
#           +-----------------------+-------------------------+------------------------+
#
#   A continuación se describen los métodos que se implementaron en ésta clase:
#
#                                             Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |  print_notification()  |  - datetime: fecha en que|  - Imprime el mensa-  |
#           |                        |     se envió el mensaje. |    je recibido.       |
#           |                        |  - id: identificador del |                       |
#           |                        |     dispositivo que      |                       |
#           |                        |     envió el mensaje.    |                       |
#           |                        |  - value: valor extremo  |                       |
#           |                        |     que se desea notifi- |                       |
#           |                        |     car.                 |                       |
#           |                        |  - name_param: signo vi- |                       |
#           |                        |     tal que se desea no- |                       |
#           +------------------------+--------------------------+-----------------------+
#           |  print_medicine_       |  - datetime: fecha en que|  - Imprime el mensa-  |
#           |  remind_notification() |     se envió el mensaje. |    je recibido.       |
#           |                        |  - id: identificador del |                       |
#           |                        |     dispositivo que      |                       |
#           |                        |     envió el mensaje.    |                       |
#           |                        |  - med_name:             |                       |
#           |                        |     nombre de la medi-   |                       |
#           |                        |     cina.                |                       |
#           |                        |  - med_dosage:           |                       |
#           |                        |     dosis que debe admi- |                       |
#           |                        |     trarse.              |                       |
#           |                        |  - time:                 |                       |
#           |                        |     hora a la que debe   |                       |
#           |                        |     aplicarse el medica- |                       |
#           |                        |     mento.               |                       |
#           |                        |  - name_param: signo vi- |                       |
#           |                        |     tal que se desea no- |                       |
#           +------------------------+--------------------------+-----------------------+
#           |   format_datetime()    |  - datetime: fecha que se|  - Formatea la fecha  |
#           |                        |     formateará.          |    en que se recibió  |
#           |                        |                          |    el mensaje.        |
#           +------------------------+--------------------------+-----------------------+
#
#-------------------------------------------------------------------------


class Monitor:

    def print_notification(self, datetime, id, value, name_param, model):
        print ("  ---------------------------------------------------")
        print ("    ADVERTENCIA")
        print ("  ---------------------------------------------------")
        print ("    Se ha detectado " + str(name_param) + " (" + str(value) + ")" + " a las " + str(self.format_datetime(datetime)) + " en el adulto mayor que utiliza el dispositivo " + str(model) + ":" + str(id))
        print ("")
        print ("")


    def print_medicine_remind_notification(self, datetime, id, med_name, med_dosage, time, model):
        print ("  ---------------------------------------------------")
        print ("    AVISO - RECORDATORIO DE TOMA DE MEDICINA")
        print ("  ---------------------------------------------------")
        print ("    ("+str(self.format_datetime(datetime))+")>"+"\n"+"    Medicina: " + 
            str(med_name) + "\n"+"    Dosis: " + str(med_dosage) +"\n"+ "    HORA DE TOMA: "+str(time)+
            "\n"+"    Para el adulto mayor que utiliza el dispositivo " + str(model) + ":" + str(id))
        print ("")
        print ("")

    def format_datetime(self, datetime):
        values_datetime = datetime.split(':')
        f_datetime = values_datetime[3] + ":" + values_datetime[4] + " del " + \
            values_datetime[0] + "/" + \
            values_datetime[1] + "/" + values_datetime[2]
        return f_datetime

    
