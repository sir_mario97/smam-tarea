# Suscriptores

Esta carpeta contiene los suscriptores del SMAM.

## Versión

2.1.0 - Mayo 2019

## Autores:
* **Brenda Yasmin Barrios Becerra**
* **Paola Torres Macías**
* **Mario Alberto Muro Barraza**

## Autores Iniciales:

* **Perla Velasco**
* **Yonathan Martínez**
