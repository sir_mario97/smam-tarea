#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: procesador_de_recordatorios.py
# Capitulo: 3 Estilo Publica-Subscribe
# Autor <StarLab> :
#   -Brenda Yasmin Barrios Becerra
#   -Paola Torres Macías
#   -Mario Alberto Muro Barraza
# Version: 1.0.1 Abril 2019
# Descripción:
#
#   Esta clase define el rol de un suscriptor, es decir, es un componente que recibe mensajes.
#
#   Las características de ésta clase son las siguientes:
#
#                                  procesador_de_recordatorios.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |                       |                         |  - Se suscribe a los   |
#           |                       |                         |    eventos generados   |
#           |                       |  - Procesar todos los   |    por el wearable     |
#           |     Procesador de     |    recordatorios de     |    Xiaomi My Band.     |
#           |     Recordatorios     |    toma de las medi-    |  - Notifica al monitor |
#           |                       |    cinas.               |    de todos los recor- |
#           |                       |                         |    datorios.           |
#           +-----------------------+-------------------------+------------------------+
#
#   A continuación se describen los métodos que se implementaron en ésta clase:
#
#                                               Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |                        |                          |  - Recibe los re-     |
#           |       consume()        |          Ninguno         |    cordatorios de me- |
#           |                        |                          |    dicinas enviados   |
#           |                        |                          |    por distribuidor   |
#           |                        |                          |    de mensajes.       |
#           +------------------------+--------------------------+-----------------------+
#           |                        |  - ch: propio de Rabbit. |  - Procesa y detecta  |
#           |                        |  - method: propio de     |    todos los recorda- |
#           |                        |     Rabbit.              |    torios.            |
#           |       callback()       |  - properties: propio de |                       |
#           |                        |     Rabbit.              |                       |
#           |                        |  - body: mensaje recibi- |                       |
#           |                        |     do.                  |                       |
#           +------------------------+--------------------------+-----------------------+
#           |    string_to_json()    |  - string: texto a con-  |  - Convierte un string|
#           |                        |     vertir en JSON.      |    en un objeto JSON. |
#           +------------------------+--------------------------+-----------------------+
#
#
#           Nota: "propio de Rabbit" implica que se utilizan de manera interna para realizar
#            de manera correcta la recepcion de datos, para éste ejemplo no shubo necesidad
#            de utilizarlos y para evitar la sobrecarga de información se han omitido sus
#            detalles. Para más información acerca del funcionamiento interno de RabbitMQ
#            puedes visitar: https://www.rabbitmq.com/
#
#-------------------------------------------------------------------------
import sys
sys.path.append('../')
from monitor import Monitor
import pika
import time
import logging


class ProcesadorRecordatorios:

    def consume(self):
        try:
            logging.basicConfig()
            # Url que define la ubicación del Distribuidor de Mensajes
            url = 'amqp://oevvxuqp:D6vn6A9ErigVUrxOINL-ok-vdD610S_I@wombat.rmq.cloudamqp.com/oevvxuqp'
            # Se utiliza como parámetro la URL dónde se encuentra el Distribuidor
            # de Mensajes
            params = pika.URLParameters(url)
            params.socket_timeout = 5
            # Se establece la conexión con el Distribuidor de Mensajes
            connection = pika.BlockingConnection(params)
            # Se solicita un canal por el cuál se enviarán los signos vitales
            channel = connection.channel()
            # Se declara una cola para leer los mensajes enviados por el
            # Publicador
            channel.queue_declare(queue='remind', durable=True)
            channel.basic_qos(prefetch_count=1)
            channel.basic_consume(self.callback, queue='remind')
            channel.start_consuming()  # Se realiza la suscripción en el Distribuidor de Mensajes
        except (KeyboardInterrupt, SystemExit):
            channel.close()  # Se cierra la conexión
            sys.exit("Conexión finalizada...")
            time.sleep(1)
            sys.exit("Programa terminado...")

    def callback(self, ch, method, properties, body):
        json_message = self.string_to_json(body)
        monitor = Monitor()
        monitor.print_medicine_remind_notification(json_message['datetime'], json_message['id'], 
            json_message['medicine_name'], json_message['medicine_dosage'], json_message['rmd_time'],
            json_message['model'])
        time.sleep(2)
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def string_to_json(self, string):
        message = {}
        string = string.replace('{', '')
        string = string.replace('}', '')
        values = string.split(', ')
        for x in values:
            v = x.split(': ')
            message[v[0].replace('\'', '')] = v[1].replace('\'', '')
        return message

if __name__ == '__main__':
    p_recordatorio = ProcesadorRecordatorios()
    p_recordatorio.consume()
